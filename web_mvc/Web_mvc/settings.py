import os  # isort:skip
import pickle
from django.utils.translation import ugettext_lazy as _

from .utils import load_dss

gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DS_SETTINGS_FILENAME = "settings.dss"

THUMBNAIL_HIGH_RESOLUTION = True
FILE_UPLOAD_PERMISSIONS = 0o644
FILE_UPLOAD_MAX_MEMORY_SIZE = 10000000 #10Mo

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False if str(os.environ.get('DEBUG', "false")) == "false" else True

ALLOWED_HOSTS = [host for host in os.environ.get('ALLOWED_HOSTS', "" if DEBUG == False else "*").replace(", ", ",").split(",")] #[]

# Application definition
ROOT_URLCONF = 'Web_mvc.urls'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

TIME_ZONE = os.environ.get('TIME_ZONE', "Europe/Zurich")
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(DATA_DIR, 'static')
MEDIA_ROOT = os.path.join(DATA_DIR, 'media')

SITE_ID = 1


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
#            os.path.join(BASE_DIR, 'theme', 'templates'),
            os.path.join(BASE_DIR, 'Web_mvc', 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.template.context_processors.static',
                'cms.context_processors.cms_settings',
#                'theme.context_processors.theme_licence',
            ],
            'libraries': {
                'sorl_thumbnail':'sorl.thumbnail.templatetags.thumbnail',
                'easy_thumbnails': 'easy_thumbnails.templatetags.thumbnail',
            },
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                #'django.template.loaders.eggs.Loader'
            ],
        },
    },
]


MIDDLEWARE = (
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'cms.middleware.utils.ApphookReloadMiddleware',
)

X_FRAME_OPTIONS = 'SAMEORIGIN'

INSTALLED_APPS = [
    'djangocms_admin_style',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'analytical',
    'crispy_forms',
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_text_ckeditor',
    'filer',
    'easy_thumbnails',
    'sorl.thumbnail',
    #'djangocms_column',
    'djangocms_file',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_style',
    #'djangocms_snippet',
    'djangocms_googlemap',
    'djangocms_video',
    'djangocms_icon',
    'djangocms_bootstrap4',
    'djangocms_bootstrap4.contrib.bootstrap4_alerts',
    'djangocms_bootstrap4.contrib.bootstrap4_badge',
    'djangocms_bootstrap4.contrib.bootstrap4_card',
    'djangocms_bootstrap4.contrib.bootstrap4_carousel',
    'djangocms_bootstrap4.contrib.bootstrap4_collapse',
    'djangocms_bootstrap4.contrib.bootstrap4_content',
    'djangocms_bootstrap4.contrib.bootstrap4_grid',
    'djangocms_bootstrap4.contrib.bootstrap4_jumbotron',
    'djangocms_bootstrap4.contrib.bootstrap4_link',
    'djangocms_bootstrap4.contrib.bootstrap4_listgroup',
    'djangocms_bootstrap4.contrib.bootstrap4_media',
    'djangocms_bootstrap4.contrib.bootstrap4_picture',
    'djangocms_bootstrap4.contrib.bootstrap4_tabs',
    'djangocms_bootstrap4.contrib.bootstrap4_utilities',
    'aldryn_apphooks_config',
#    'cmsplugin_filer_image',
    'parler',
    'taggit',
    'taggit_autosuggest',
    'meta',
    'sortedm2m',
    'djangocms_page_meta',
    'djangocms_blog',
    'djangocms_forms',
    'djangocms_transfer',
    'Web_mvc',
    'visualstudio',
]

LANGS = os.environ.get("LANGUAGES", "fr-fr").replace(", ", ",").split(",")

USE_I18N = True #True if len(LANGS) > 1 else False

USE_L10N = USE_I18N

LANGUAGES = []
for lang in LANGS:
    LANGUAGES.append((lang, gettext(lang)))

LANGUAGE_CODE = LANGS[0]

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

META_SITE_PROTOCOL = 'https'
META_USE_SITES = True

CMS_LANGUAGES = {
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_LANGUAGES[1] = []

for lg in LANGS:
    CMS_LANGUAGES[1].append({
            'code': lg,
            'hide_untranslated': False,
            'name': lg,
            'public': True,
        })

if len(LANGS) > 1:
    CMS_LANGUAGES[1][1]['redirect_on_fallback'] = True


DJANGOCMS_FORMS_PLUGIN_NAME = _('Formulaire')

CRISPY_TEMPLATE_PACK = 'bootstrap4'

DJANGOCMS_FORMS_WIDGET_CSS_CLASSES = {'__all__': ('form-control', ) }


CMS_PERMISSION = bool(os.environ.get('CMS_PERMISSION', False))

CMS_PLACEHOLDER_CONF = {}

if not os.environ.get('DEV', False):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': '/var/log/Web_mvc.log',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['file'],
                'level': 'DEBUG',
                'propagate': True,
            },
        },
    }

DATABASES = {}

if os.environ.get('POSTGRES_HOST', None) == None:
    default_db = {
        'ENGINE': "django.db.backends.sqlite3",
        'NAME': os.path.join(DATA_DIR, 'db.sqlite3'),
    }
elif os.environ.get('POSTGRES_HOST', None):
    default_db = {
            'ENGINE': "django.db.backends.postgresql_psycopg2",
            'HOST': os.environ.get('POSTGRES_HOST'),
            'NAME': os.environ.get('POSTGRES_NAME'),
            'USER': os.environ.get('POSTGRES_USER', os.environ.get('POSTGRES_NAME')),
            'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
            'PORT': "5432",
            }
    
DATABASES['default'] = default_db

SEND_GRID_API_KEY = ''
EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL')
ACCOUNT_EMAIL_SUBJECT_PREFIX = ''
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_EMAIL_REQUIRED=True
ACCOUNT_EMAIL_VERIFICATION= os.environ.get('ACCOUNT_EMAIL_VERIFICATION', "mandatory")
ACCOUNT_DEFAULT_HTTP_PROTOCOL="https"
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE=True
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = "account/login"

LOCALE_PATHS = ["Web_mvc/locale"]
# os.path.join(BASE_DIR, 'theme/locale')

BLOG_PERMALINK_URLS = {
    'slug': r'^(?P<slug>\w[-\w]*)/$',
    'full_date': r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<slug>\w[-\w]*)/$',
    'short_date': r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<slug>\w[-\w]*)/$',
    'category': r'^(?P<category>\w[-\w]*)/(?P<slug>\w[-\w]*)/$',
}

FILER_FILE_MODELS = (
    'filer.Image',
    'filer.File',
)

# Analytical

CHARTBEAT_USER_ID = os.environ.get("CHARTBEAT_USER_ID", None)
GOOGLE_ANALYTICS_PROPERTY_ID = os.environ.get("GOOGLE_ANALYTICS_PROPERTY_ID", None)
CLICKMAP_TRACKER_CODE = os.environ.get("CLICKMAP_TRACKER_CODE", None)
CLICKY_SITE_ID = os.environ.get("CLICKY_SITE_ID", None)
CRAZY_EGG_ACCOUNT_NUMBER = os.environ.get("CRAZY_EGG_ACCOUNT_NUMBER", None)
FACEBOOK_PIXEL_ID = os.environ.get("FACEBOOK_PIXEL_ID", None)
GAUGES_SITE_ID = os.environ.get("GAUGES_SITE_ID", None)
HUBSPOT_PORTAL_ID = os.environ.get("HUBSPOT_PORTAL_ID", None)
HUBSPOT_DOMAIN = os.environ.get("HUBSPOT_DOMAIN", None)
INTERCOM_APP_ID = os.environ.get("INTERCOM_APP_ID", None)
KISS_INSIGHTS_ACCOUNT_NUMBER = os.environ.get("KISS_INSIGHTS_ACCOUNT_NUMBER", None)
KISS_INSIGHTS_SITE_CODE = os.environ.get("KISS_INSIGHTS_SITE_CODE", None)
KISS_METRICS_API_KEY = os.environ.get("KISS_METRICS_API_KEY", None)
MATOMO_DOMAIN_PATH = os.environ.get("MATOMO_DOMAIN_PATH", None)
MATOMO_SITE_ID = os.environ.get("MATOMO_SITE_ID", None)
MIXPANEL_API_TOKEN = os.environ.get("MIXPANEL_API_TOKEN", None)
OLARK_SITE_ID = os.environ.get("OLARK_SITE_ID", None)
OPTIMIZELY_ACCOUNT_NUMBER = os.environ.get("OPTIMIZELY_ACCOUNT_NUMBER", None)
PERFORMABLE_API_KEY = os.environ.get("PERFORMABLE_API_KEY", None)
RATING_MAILRU_COUNTER_ID = os.environ.get("RATING_MAILRU_COUNTER_ID", None)
WOOPRA_DOMAIN = os.environ.get("WOOPRA_DOMAIN", None)
YANDEX_METRICA_COUNTER_ID = os.environ.get("YANDEX_METRICA_COUNTER_ID", None)

# Dynamicly stored settings
DS_SETTINGS = load_dss(DATA_DIR, DS_SETTINGS_FILENAME)

if DS_SETTINGS != None:
    INSTALLED_APPS += DS_SETTINGS['settings'].get('INSTALLED_APPS')
    LOCALE_PATHS += [os.path.join(BASE_DIR, local_path) for local_path in DS_SETTINGS['settings'].get('LOCALE_PATHS')]
    DJANGOCMS_FORMS_TEMPLATES = DS_SETTINGS['settings'].get('DJANGOCMS_FORMS_TEMPLATES')
    NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS = DS_SETTINGS['settings'].get('NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS')
    TEMPLATES[0]['DIRS'] += [os.path.join(BASE_DIR, local_path) for local_path in DS_SETTINGS['settings']['TEMPLATES'].get('DIRS')]
    TEMPLATES[0]['OPTIONS']['context_processors'] += DS_SETTINGS['settings']['TEMPLATES']['OPTIONS'].get('context_processors', [])
    BLOG_IMAGE_FULL_SIZE = DS_SETTINGS['settings'].get('BLOG_IMAGE_FULL_SIZE')
    BLOG_IMAGE_THUMBNAIL_SIZE = DS_SETTINGS['settings'].get('BLOG_IMAGE_THUMBNAIL_SIZE')
    PORTFOLIO_IMAGE_THUMBNAIL_SIZE = DS_SETTINGS['settings'].get('PORTFOLIO_IMAGE_THUMBNAIL_SIZE')
    BLOG_LATEST_POSTS = DS_SETTINGS['settings'].get('BLOG_LATEST_POSTS')
    BLOG_PLUGIN_TEMPLATE_FOLDERS = DS_SETTINGS['settings'].get('BLOG_PLUGIN_TEMPLATE_FOLDERS')
    CMS_TEMPLATES = DS_SETTINGS['settings'].get('CMS_TEMPLATES')
    CKEDITOR_SETTINGS = DS_SETTINGS['settings'].get('CKEDITOR_SETTINGS')
    DJANGOCMS_ICON_SETS = DS_SETTINGS['settings'].get('DJANGOCMS_ICON_SETS')
    SECTION_DEFAULT_ATTRIBUTES = DS_SETTINGS['settings'].get('SECTION_DEFAULT_ATTRIBUTES')
    SECTION_INVERTED_ATTRIBUTES = DS_SETTINGS['settings'].get('SECTION_INVERTED_ATTRIBUTES')
