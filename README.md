# Web.mvc

Quickly create secure and open-source websites.

## Framework
### Model-View-Controller
Web.mvc uses the Model-View-Controller design pattern, which is commonly used for developing user interfaces and has become very popular for web devloppment.
### Django-CMS
This Content Managment System based on [Django](https://docs.djangoproject.com/) is a powerful tool to easily create content for your website.

More information [@Django-CMS](http://docs.django-cms.org/en/latest/).
### Bootstrap 4
In order to modify content from the CMS, Bootstrap 4 as been choosen to stadardize HTML components. It also makes it easier to create your own templates.

Get more information about components [@Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/introduction/).
## Templates
Web.mvc is the core technology to create a website. It doesn't come with templates.

Templates and their configurations come bundled inside a Web.mvc Theme.

Since I'm not a designer, I use Bootstrap Templates which I don't own to create Web.mvc Themes. For that reason, I cannot share code for those Themes.

You can nonetheless use our blank Theme to easely create your own.

### Web.mvc Base Theme
Bootstrap your own theme easily with Web.mvc Base Theme.

You can create your theme from scratch or use a Bootstrap 4 template. They are widely avalible on Internet.

Checkout the repo to get started [@Web.mvc-base-theme](https://gitlab.com/waser-technologies/web.mvc-themes/web.mvc-base-theme)

## Hosting
Web.mvc is powered by Docker to allow you to easily build it anywhere you want.

Since Web.mvc doesn't come with templates you can't deploy Web.mvc on it's own. Checkout the [Deploy Web.mvc](https://gitlab.com/waser-technologies/web.mvc-themes/web.mvc-base-theme#deploy-webmvc) section of [Web.mvc-base-theme](https://gitlab.com/waser-technologies/web.mvc-themes/web.mvc-base-theme).

If you need to modify Web.mvc source code, publish a [Docker](https://docs.docker.com/get-docker/) image of your version of Web.mvc. 

You would need an [Docker Hub](https://hub.docker.com) free account and to change thoses variables accordingly inside the .env file.

```
DOCKER_USER
DOCKER_PASSWORD
DOCKER_REPO
```
Now you can push your image to your docker repository.

`make image`

Don't forget to update the image name inside your theme's `Dockerfile`.

## Contributing
### Submitting code
If you modified Web.mvc source code, please feel free to submit your contribution for everyone to enjoy.

