include ./.env

clone:
	git clone $(GIT_PATH) .

pull:
	git pull $(GIT_PATH)

env:
	pip install --upgrade pip
	pip install -r './web_mvc/requirements/base.txt'
	pip install -r './web_mvc/requirements/git.txt'

image:
	docker build -t $(DOCKER_IMAGE) .
	docker login -u $(DOCKER_USER) -p $(DOCKER_PASSWORD)
	docker push $(DOCKER_IMAGE)

static:
	python ./web_mvc/manage.py collectstatic --no-input

init:
	python ./init_dss.py
	python ./web_mvc/manage.py migrate --no-input
	python ./web_mvc/manage.py collectstatic --no-input
	python ./web_mvc/manage.py createsuperuser

migrations:

	python ./web_mvc/manage.py makemigrations --no-input

migrate:

	python ./web_mvc/manage.py migrate --no-input

server:
	python ./web_mvc/manage.py runserver

dss:
	python ./init_dss.py